﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Web.Services;

using HME.SQLSERVER.DAL;
using System.IO;

namespace AuthCode
{
    public partial class GetAthorization : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Get_Athorization_Code("", "", "", "", "", "", "");
        }

        private void Get_Athorization_Code(string PartNumber, string SerialNumber, string InstallID, string CustomerID, string ContactName, string ContactPhone, string CompanyCode)
        {
            string connString = ConfigurationManager.ConnectionStrings["AX2012_DEV"].ConnectionString;

            dbSQL db = new dbSQL(connString);
            if (db == null)
                return;

            string authWorkingDirectory = @"d:\auths" + "\\";// ConfigurationManager.AppSettings["AuthWorkingDirectory"];
            string authCODE = ConfigurationManager.AppSettings["AuthCODE"];
            string authExe = ConfigurationManager.AppSettings["AuthExe"];

            if (File.Exists(authWorkingDirectory + authCODE))
            {
                File.Delete(authWorkingDirectory + authCODE);
            }

            ProcessStartInfo psinfo = new ProcessStartInfo(authWorkingDirectory + authExe, "EDD70351 49V09418 8");
            psinfo.UseShellExecute = false;
            psinfo.RedirectStandardOutput = true;
            psinfo.RedirectStandardInput = true;
            psinfo.RedirectStandardError = true;
            psinfo.Verb = "runas";
            Process process = new Process();
            //process.Start(@"\\powweb\d$\auths\hme.exe", "EDD70351 49V09418 8");
            //var proc = System.Diagnostics.Process.Start(@"\\powweb\d$\auths\hme.exe", "EDD70351 49V09418 8");
            process.StartInfo = psinfo;
            process.Start();
            System.Threading.Thread.Sleep(3000);
            process.Kill();
            //proc.Kill();

            if (File.Exists(authWorkingDirectory + authCODE))
            {
                File.Delete(authWorkingDirectory + authCODE);
            }

        }

        [WebMethod]
        public static void GetAthorizationCode(string PartNumber, string SerialNumber, string InstallID, string CustomerID, string ContactName, string ContactPhone, string CompanyCode)
        {
 
        }
    }
}
